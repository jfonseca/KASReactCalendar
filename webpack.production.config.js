const webpack = require('webpack');
const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin');

const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractSass = new ExtractTextPlugin({
    // filename: "[name].[contenthash].css",
    filename: "styles.css",
    disable: process.env.NODE_ENV === "development"
});

module.exports = {
  devtool: 'cheap-source-map',
  entry: path.resolve(__dirname, 'src/index.js'),
  output: {
    path: `${__dirname}/build`,
    publicPath: '/',
    filename: './bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      }, {
        test: /\.svg$/,
        loader: 'svg-inline-loader'
      },
      {
        test: /\.scss$/,
        use: extractSass.extract({
            use: ["css-loader","sass-loader"],
            // use style-loader in development
            fallback: "style-loader"
        })
      }
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  optimization: {
    minimize: true,
    minimizer: [new UglifyJsPlugin({
        sourceMap: true,
        uglifyOptions: {
          compress: {
            warnings: false
          }
        }
      })]
  },
  plugins: [extractSass,
  new CopyWebpackPlugin([
      {
        from: './src/index.html',
        to: 'index.html'
      }, {
        from: './src/assets/',
        to: 'images'
      }
    ])]
};
