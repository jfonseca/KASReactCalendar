import React, { Component } from 'react';
import Calendar from './Sections/Calendar';
import { HashRouter as Router, Route, hashHistory, browserHistory} from "react-router-dom";
import moment from "moment";

import './App.scss';

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      
      <div >
        <Calendar />
      </div>

    );
  }
}

export default App;
