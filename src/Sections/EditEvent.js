import React, { Component } from "react";
import DatePicker from "react-datepicker";
import "./react-datepicker.scss";
import {checkEventsOnTimes} from "../Models/CalendarHelper";
import moment from "moment";
import { update, deleteItem } from "../Models/Storage";

//CSS
import "./AddEvent.scss";

class EditEvent extends Component {

  constructor(props) {
    super(props);

    this.state = {
      selected: moment(this.props.item.startDate),
      title: "",
      message: this.props.item.message,
      update: props.updateCal(),
      close: props.close(),
      error: false,
      exclude: checkEventsOnTimes(moment(this.props.item.startDate)),
      props: this.props.item
    };
    //console.log("Edit Panel ");
  }

  // Excludes the times on selected day
  componentDidMount() {
    this.setState({
      exclude: checkEventsOnTimes(
        moment(this.props.item.startDate)
      )
    });
  }

  // Selection in calendar changed
  dateChange = day => {
    let exclude = checkEventsOnTimes(day);
    this.setState({ selected: day, exclude: exclude });
  };

  // Update when message changes and checks for blank
  messageChange = event => {
    this.setState({ message: event.target.value });
    if (this.state.error && this.state.message.length > 1) {
      this.setState({ error: false });
    }
  };

  // updates event to localstorage and check for message blank
  updateEvent = event => {
    //console.log("Save Event");
    if (this.state.message.length < 1) {
      this.setState({ error: true });
      return;
    }
    update("KAS.events", {
      id: this.state.props.id,
      startDate: this.state.selected,
      endDate: moment(this.state.selected).add(1, "hour"),
      message: this.state.message
    });
    this.state.update();
  }

  // Delete event by ID
  deleteEvent = event => {
    //console.log("Delete Event");
    deleteItem("KAS.events", this.state.props.id);
    this.setState({ selected: this.state.selecteds });
    this.state.update();
  }

  // Form div and classname swap depended if error
  messageInput = () => {
    var className = this.state.error ? "input highlight" : "input";
    return (
      <form className="message"  onSubmit={e => { e.preventDefault();}} >
        <input
          className={className}
          type="text"
          placeholder="Enter Title Here"
          onChange={this.messageChange}
          value={this.state.message}
        />
      </form>
    );
  };

  render() {
    return (
      <div className="popup">
        <div className="popup_inner">
          <div className="header">Edit Event</div>
          <div className="subHeader" />
          <div className="wrapper">
            {this.messageInput()}
            <div className="datepicker">
              <DatePicker
                inline
                selected={this.state.selected}
                onChange={this.dateChange}
                showTimeSelect
                timeFormat="h:A"
                timeIntervals={60}
                dateFormat="LLL"
                timeCaption="time"
                minDate={moment()}
                excludeTimes={this.state.exclude}
                maxDate={moment().add(5, "months")}
                showDisabledMonthNavigation
                placeholderText="Event Date"
              />
            </div>
          </div>
          <div className="editFooter">
            <div className="delete btnDelete" onClick={this.deleteEvent.bind(this)}>DELETE</div>
            <div className="close btn" onClick={this.props.close()}>CLOSE</div>
            <div className="save btnSave" onClick={this.updateEvent.bind(this)}>SAVE</div>
          </div>
        </div>
      </div>
    );
  }
}

export default EditEvent;
