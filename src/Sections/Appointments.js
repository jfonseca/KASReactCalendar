import React, { Component } from "react";
import { addAppointimentEvent } from "../Views/CalendarViews";
import Edit from "./EditEvent";
import {checkEventsOnDay, getEvent } from "../Models/CalendarHelper";
import moment from "moment";

//CSS
import "./Appointments.scss";

class Appointment extends Component {
  

  constructor(props) {
    super(props);
    
    this.state = {
        isPopupActive: false,
        selectedDay: props.date.rawDate,
        month:props.date.month,
        day:props.date.day,
        events:props.date.events,
        selected: {},
        update: props.updateCal(),
    }

    //console.log("Appointment Component ");
  }

  // Updates with the main calendar updates
  componentWillReceiveProps(props) {
    //console.log("Appointment Book Updated");
      this.setState({
        selectedDay: props.date.rawDate,
        month:props.date.month,
        day:props.date.day,
        events:checkEventsOnDay(props.date.rawDate)
      });
  }

  // Close Edit panel
  closePopup = () => {
    let popTemp = !this.state.isPopupActive;
    this.setState({isPopupActive: false});
  }

  // Open Edit panel 
  editEvent = (event) => {
    //console.log('Edit Event', event);
    let daySelected = getEvent(event.currentTarget.getAttribute('event'));
    this.setState({isPopupActive: true, selected: daySelected});
  }

  // Update Main Calendar
  updateCalendar = () => {
    //console.log("Update Calendar ");
    this.setState({isPopupActive: false});
    this.state.update();
  }
  
  render() {
    var Popup = null;
    if(this.state.isPopupActive){
        Popup = <Edit close={() => this.closePopup} updateCal={() => this.updateCalendar} item={this.state.selected} />;
      }
    return (
      <div className="appointments">
       {Popup}
        <div className="header">{this.state.day}</div>
        <div className="subHeader">
          <div className="appoint">
            <div className="appoint-label">{this.state.month}</div>
          </div>
        </div>
        <div className="events">
          {this.state.events.map((event, i) =>
            addAppointimentEvent(event, i,  this.editEvent.bind(this))
          )}
        </div>
      </div>
    );
  }
}

export default Appointment;
