import Appointment from "../Appointments";
import React from "react";
import moment from "moment";
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });

import { shallow, mount, render } from "enzyme";

const mockCallback = jest.fn();
const params = {
  rawDate: moment(),
  day: "2",
  month: "12",
  events: [
    {
      id: "6f13a2f2-4046-4a86-aaea-9fde2878e2e3",
      startDate: "2018-03-14T05:41:11.000Z",
      endDate: "2018-03-14T06:41:11.000Z",
      message: "Welcome Friends"
    },
    {
      id: "bb19de82-6a66-426b-8a81-0353c1399267",
      startDate: "2018-03-21T05:42:54.000Z",
      endDate: "2018-03-21T05:42:54.000Z",
      message: "TEST"
    }
  ],
  current: moment()
};

test("Appointment Mounted and built event array", () => {
  const wrapper = shallow(
    <Appointment date={params} updateCal={mockCallback} />
  );
  const events = wrapper.find(".appoint");
  // events.last().simulate('click');

  expect(events.length).toBe(3);
});
