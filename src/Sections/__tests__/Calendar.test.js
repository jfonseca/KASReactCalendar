import Calendar from '../Calendar';
import React from 'react';

import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import { shallow, mount, render  } from 'enzyme';

test('Check if calendar was created', () => {
  const wrapper = mount(<Calendar />);

    //TODO  const week = wrapper.find('.week');
    const day = wrapper.find('.day');

    expect(day.length).toBe(49);

});