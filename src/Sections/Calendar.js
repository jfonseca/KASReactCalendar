import React, { Component } from "react";
import moment from "moment";
import { chunk, range } from "lodash";

import {getCalendarArray, todaysObj } from "../Models/CalendarHelper";
import { addDay, dayOfWeek } from "../Views/CalendarViews";

//CSS
import "./Calendar.scss";

//Components
import Appointment from "./Appointments";
import AddEvent from "./AddEvent";
import EventTracker from "./EventTracker";

class Calendar extends Component {

  constructor(props) {
    super(props);

    const momentLib = moment();

    this.days = getCalendarArray(momentLib);
    this.month = moment().format("MMMM");
    this.today = moment();

    this.state = {
      selected: {
        rawDate: momentLib,
        day: momentLib.format("D"),
        month: momentLib.format("MMMM"),
        year: momentLib.format("YYYY"),
        monthType: "current",
        hasEvent: false,
        events: [],
        current: momentLib
      },
      isPopupActive: false
    };
  }

  // Set start state to display Today
  componentDidMount() {
    //console.log("Component Mount");
    this.setState({ selected: todaysObj });
  }

  // Day on was clicked on calendar
  dayClick = day => {
    this.setState({ selected: day });
  }

  // This shows or removes the popup for Add Panel
  popupAddClose = () => {
    let popTemp = !this.state.isPopupActive;
    this.setState({ isPopupActive: popTemp });
  }

  // Updates calendar and checks if new event were added
  updateCalendar = () => {
    //console.log("Calendar Update");
    this.days = getCalendarArray(moment());
    this.setState({ isPopupActive: false });
  };

  render() {
    let Popup = null;
    if (this.state.isPopupActive) {
      Popup = (
        <AddEvent
          close={() => this.popupAddClose}
          updateCal={() => this.updateCalendar}
        />
      );
    }
    return (
      <div className="App">
        {Popup}
        <div className="calendar">
          <div className="month">{this.month}</div>

          <div className="topnav">
            <EventTracker update={this.state.selected} />
            <div className="add btn" onClick={this.popupAddClose}>ADD EVENT</div>
          </div>

          {dayOfWeek()}
          {chunk(this.days, 7).map((row, weekNum) => (
            <div key={weekNum} className="week">
              {row.map(day =>
                addDay(day, this.dayClick.bind(this, day))
              )}
            </div>
          ))}
        </div>
        <Appointment date={this.state.selected} updateCal={() => this.updateCalendar} />
      </div>
    );
  }
}

export default Calendar;
