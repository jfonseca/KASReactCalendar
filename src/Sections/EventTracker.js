import React, { Component } from "react";
import { getAllEvents } from "../Models/CalendarHelper";
import moment from "moment";
require("moment-precise-range-plugin");
import { sortBy } from "lodash";

//CSS
import "./EventTracker.scss";

class EventTracker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nextEvent: ""
    };
    this.interval = null;
    this.update = this.update.bind(this);
  }

  update = () => {

    let events = getAllEvents();
    if (!events.length != 0) {
      
        return;
    }
    //
    let today = new Date();

    // Filter events to show after current time
    let currentTime = events.filter(function(d) {
      return new Date(d.startDate) - today > 0;
    });

    // Sort event
    currentTime = currentTime.sort(function(a, b) {
      return new Date(a.startDate).getTime() - new Date(b.startDate).getTime();
    });

    
    //Get the time different from current and next event
    let next = moment.preciseDiff(today, currentTime[0].startDate, true);

    //Text for panel
    let preMessage = "* " + currentTime[0].message + ", is your next event in ";
    let duration =
      next.years > 0
        ? preMessage + next.years + " Years"
        : next.months > 0
          ? preMessage + next.months + " Month"
          : next.days > 0
            ? preMessage + next.days + " Days, " + next.hours + " Hours"
            : next.hours > 0
              ? preMessage + next.hours + " Hours"
              : next.minutes > 0
                ? preMessage + next.minutes + " Minutes"
                : next.seconds > 0
                  ? preMessage + next.minutes + " Minutes"
                  : "No Events at this current time";

    
    //Update state
    this.setState({
      nextEvent: duration
    });
  };

  componentWillReceiveProps(props) {
    //console.log("Event Tracker Update");
  }

  componentDidMount() {
    //start the timer
    let timer = setInterval(() => this.update(), 3000);
    this.setState({ timer });
    this.update();
  }

  componentWillUnmount() {
    //delete timer beofre unmount
    clearInterval(this.state.timer);
  }

  render() {
    return <div className="eventTracker alert">{this.state.nextEvent}</div>;
  }
}

export default EventTracker;
