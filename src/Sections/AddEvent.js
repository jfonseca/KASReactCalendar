import React, { Component } from "react";
import DatePicker from "react-datepicker";
import "./react-datepicker.scss";
import {checkEventsOnTimes } from "../Models/CalendarHelper";
import moment from "moment";
import CalendarViews from "../Views/CalendarViews";
import { push } from "../Models/Storage";


import uuid from "uuid";

//CSS
import "./AddEvent.scss";

class AddEvent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selected: moment(),
      title: "",
      message: "",
      update: props.updateCal(),
      close: props.close(),
      error: false,
      exclude: checkEventsOnTimes(moment())
    };
    //console.log('Add Event Panel ');
  }

  componentDidMount() {
    this.setState({exclude: checkEventsOnTimes(moment())});
  }

  // Selection in calendar changed
  dateChange = day => {
    let exclude = checkEventsOnTimes(day);
    this.setState({selected: day, exclude: exclude});
  }

  // Update when message changes and checks for blank
  messageChange = (event) => {
    this.setState({message: event.target.value});
    if(this.state.error && this.state.message.length > 1){
      this.setState({error: false});
    }
  }
  
  // Save event to localstorage and check for message blank
  saveEvent = (event) => {
    if(this.state.message.length < 1){
      this.setState({error: true});
      return;
    }
    let setTime = moment(this.state.selected);
    setTime.set({'minute': 0, 'seconds':0, 'millisecond':0});
    push("KAS.events",{id: uuid.v4(), startDate:setTime, endDate:moment(this.state.selected).add(1, 'hour'), message:this.state.message});
    this.state.update();
  }

  // Form div and classname swap depended if error
  messageInput = () => { 
    var className = this.state.error ? 'input highlight' : 'input';
    return (
    <form className='message' onSubmit={e => { e.preventDefault();}} >
        <input
          className={className}
          type="text"
          placeholder="Enter Title Here"
          onChange={this.messageChange}
          value={this.state.message}
        />
    </form>
  )}

  render() {

    return (
      <div className="popup">
        <div className="popup_inner">
          <div className="header">Add Event</div>
          <div className="subHeader"></div>
          <div className="wrapper">
          {this.messageInput()}
          <div className="datepicker" >
            <DatePicker
              inline
              selected={this.state.selected}
              onChange={this.dateChange}
              showTimeSelect
              timeFormat="h:A"
              timeIntervals={60}
              dateFormat="LLL"
              timeCaption="time"
              minDate={moment()}
              excludeTimes={this.state.exclude}
              maxDate={moment().add(5, "months")}
              showDisabledMonthNavigation
              placeholderText="Event Date"
            />
          </div>
          </div>
          <div className="footer">
            <div className="close btn" onClick={this.props.close()}>CLOSE</div>
            <div className="save btnSave" onClick={this.saveEvent.bind(this)}>SAVE</div>
          </div>
        </div>
      </div>
    );
  }
}

export default AddEvent;
