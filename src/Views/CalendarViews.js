import React, { Component } from "react";
import { dayNameOfWeek, addEvent } from "../Models/CalendarHelper";
import moment from "moment";

  // Appointment panel event displayed
  export const addAppointimentEvent = (day, i, callback) => {
    return (
      <div key={i} className="subHeader" event={day.id} onClick={callback}>
        <div className="appoint">
          <div className="appEvent">
            <div className="time">{moment(day.startDate).format("h:mmA")}</div>
            <div className="title">{day.message}</div>
            <div className="clear" />
          </div>
        </div>
      </div>
    );
  };

  // Main calendar Day displayed in the 42 days
  export const addDay = (day, callback) => {
    return (
      <div key={day.day} className={`day ${day.monthType}`} onClick={callback}>
        <h3 className="day-label">{day.day}</h3>
        <div className="eventList">{day.events.map((event, i) => addEventView(event, i))}</div>
      </div>
    );
  };

  // Main calendar Days of week bar
  export const dayOfWeek = () => (
    <div className="daysOfWeek">
      { dayNameOfWeek.map((day, i) => (
        <div key={i} className="day">
          <div className="day-label">{day}</div>
        </div>
      ))}
    </div>
  );

  // Main calendar Event displayed
  export const addEventView = (day, i) => {
    return (
      <div key={i} className="event event-start event-end">
        {day.message}
      </div>
    );
  };
