import { get, } from "./Storage";
import moment from "moment";

export let todaysObj = {};

export const dayNameOfWeek = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

// Returns all events in localStorage
export const getAllEvents = () => {
  let events = get("KAS.events");
  if (!events) {
    return [];
  }
  return sortEventArray(events);;
}

export const sortEventArray = event => {
  return event.sort(function(a, b) {
    return new Date(a.startDate).getTime() - new Date(b.startDate).getTime();
  });
}

// Returns all events on a specific day
export const checkEventsOnDay = day => {
  let events = get("KAS.events");
  if (!events) {
    return [];
  }
  let eventArr = [];

  events.forEach(function(event) {
    let eventCheck = day.isBetween(event.startDate, event.endDate);
    eventCheck = day.isSame(event.startDate, "day");
    if (eventCheck) {
      eventArr.push(event);
    }
  });

  return sortEventArray(eventArr);
}

// Returns Event that matches ID
export const getEvent = eventId => {
  let events = get("KAS.events");
  if (!events) {
      return [];
    }
    
  return events.find(function (obj) { return obj.id === eventId; });
}

// Returns what times should be excluded from calendar
export const checkEventsOnTimes = day => {
  let events = get("KAS.events");
  if (!events) {
    return [];
  }
  let eventArr = [];

  events.forEach(function(event) {
    let eventCheck = day.isSame(event.startDate, "day");
    if (eventCheck) {
      let hour = moment(event.startDate).hour();
      eventArr.push(
        moment().hours(hour-1).minutes(60)
      );
    }
  });

  return eventArr;
};

// Returns an array of objects {} for 42 days
export const getCalendarArray = momentLib => {
  let days = [];

  let startOffset = momentLib.startOf("month").day();
  let fromDate = momentLib.subtract(startOffset, "days");
  let currentDay = moment();
  let currentMonth = currentDay.format("M");

  for (let i = 0; i < 42; i++) {
    let tempDay = fromDate.clone().add(i, "day");

    let month = tempDay.format("M");

    // Get all events for this day
    let dayHasEvent = checkEventsOnDay(tempDay);

    // Check if this is the current day
    let isCurrentDay = tempDay.isSame(currentDay, "day");

    let tempObj = {
      rawDate: tempDay,
      day: tempDay.format("D"),
      month: tempDay.format("MMMM"),
      year: tempDay.format("YYYY"),
      monthType: isCurrentDay ? "current" : month == currentMonth ? "" : month < currentMonth ? "past" : "next",
      hasEvent: dayHasEvent.length > 0,
      events: dayHasEvent,
      current: currentDay
    };

    days.push(tempObj);

    if (isCurrentDay) {
      todaysObj = tempObj;
    }
  }

  return days;
};


