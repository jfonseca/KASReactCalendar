  // Save to storage
  export const set = (key, obj) => {
    if (!key || !obj) {
      return null;
    }
    return localStorage.setItem(key, JSON.stringify(obj));
  };

  // Retrieve from localStorage
  export const get = key => {
    if (!key) {
      return null;
    }
    return JSON.parse(localStorage.getItem(key));
  };

  // Update localStorage item
  export const update = (key, obj) => {
    if (!key) {
      return null;
    }

    let arrTemp = JSON.parse(localStorage.getItem(key));

    if (!arrTemp) {
      arrTemp = [];
    }
    let itemIndex = arrTemp.findIndex(item => item.id == obj.id);
    arrTemp[itemIndex] = obj;

    return localStorage.setItem(key, JSON.stringify(arrTemp));
  };

  // Delete localStorage by ID
  export const deleteItem = (key, id) => {
    if (!key) {
      return null;
    }
    let arrTemp = JSON.parse(localStorage.getItem(key));

    if (!arrTemp) {
      arrTemp = [];
    }
    let itemIndex = arrTemp.findIndex(item => item.id == id);

    arrTemp.splice(itemIndex, 1);
    return localStorage.setItem(key, JSON.stringify(arrTemp));
  };

  // Add new item to array on localStorage
  export const push = (key, obj) => {
    if (!key) {
      return null;
    }
    let arrTemp = JSON.parse(localStorage.getItem(key));

    if (!arrTemp) {
      arrTemp = [];
    }
    arrTemp.push(obj);

    return localStorage.setItem(key, JSON.stringify(arrTemp));
  };
