import { get, set, deleteItem, push } from "../Storage";
import moment from "moment";


let KEY = "KAS.events";
let VALUE = [{"id":"111","startDate":"2018-03-21T05:42:54.000Z","endDate":"2018-03-21T05:42:54.000Z","message":"Test1"},
    {"id":"222","startDate":"2018-03-21T05:50:45.000Z","endDate":"2018-03-22T05:50:45.000Z","message":"test2"},
    {"id":"333","startDate":"2018-03-20T07:27:25.000Z","endDate":"2018-03-20T08:27:25.000Z","message":"Test3"},
    {"id":"444","startDate":"2018-03-05T00:02:16.753Z","endDate":"2018-03-05T01:02:16.753Z","message":"Test4"}];

describe('storage', () =>
[localStorage, sessionStorage].map(storage => {

    beforeEach(() => {
        storage.clear();
        jest.clearAllMocks();
    });

    test('Storage.set ', () => {
        set(KEY, VALUE);
        let storageItems = get(KEY);
        expect(storageItems).toEqual(VALUE);
    });
    test('Storage.push ', () => {
        set(KEY, VALUE);
        let storageItems = get(KEY);
        push(KEY, {"id":"555","startDate":"2018-03-10T00:02:16.753Z","endDate":"2018-03-10T01:02:16.753Z","message":"Test5"});
        let afterPush = get(KEY);
        expect(afterPush).toContainEqual({"id":"555","startDate":"2018-03-10T00:02:16.753Z","endDate":"2018-03-10T01:02:16.753Z","message":"Test5"});
    });
 }));
    