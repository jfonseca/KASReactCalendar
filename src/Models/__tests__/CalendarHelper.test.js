import {getCalendarArray, getAllEvents, checkEventsOnDay, getEvent, checkEventsOnTimes } from "../CalendarHelper";
import { get, set, deleteItem, push } from "../Storage";
import moment from "moment";


let name = "KAS.events";
let list = [{"id":"111","startDate":"2018-03-21T05:42:54.000Z","endDate":"2018-03-21T05:42:54.000Z","message":"Test1"},
    {"id":"222","startDate":"2018-03-21T05:50:45.000Z","endDate":"2018-03-22T05:50:45.000Z","message":"test2"},
    {"id":"333","startDate":"2018-03-20T07:27:25.000Z","endDate":"2018-03-20T08:27:25.000Z","message":"Test3"},
    {"id":"444","startDate":"2018-03-05T00:02:16.753Z","endDate":"2018-03-05T01:02:16.753Z","message":"Test4"}];

describe('storage', () =>
[localStorage, sessionStorage].map(storage => {

    beforeEach(() => {
        storage.clear();
        jest.clearAllMocks();
    });
    test('CalendarHelper.getCalendarArray', () => {
        let days = getCalendarArray(moment());
        expect(days.length).toBe(42);
    });
    test('CalendarHelper.getAllEvents', () => {
        set(name, list);
        let events = getAllEvents();
        expect(events.length).toBe(4);
    });
    test('CalendarHelper.checkEventsOnDay', () => {
        set(name, list);
        let events = checkEventsOnDay(moment('2018-03-21T05:50:45.000Z'));
        expect(events.length).toBe(2);
    });
    test('CalendarHelper.getEvent', () => {
        set(name, list);
        let event = getEvent('333');
        expect(event).toEqual({"id":"333","startDate":"2018-03-20T07:27:25.000Z","endDate":"2018-03-20T08:27:25.000Z","message":"Test3"});
    });
    test('CalendarHelper.checkEventsOnTimes', () => {
        set(name, list);
        let event = checkEventsOnTimes(moment('2018-03-21T05:50:45.000Z'));
        expect(event.length).toBe(2);
    });
 }));
    