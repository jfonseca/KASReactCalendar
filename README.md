# KAS React Calendar

This calendar project was built in 4 days. The goal was to allow user to add, delete, and edit events. Also provide up to date information on their next event on the calendar. 

## Example

[View Calendar Online](https://www.klipartstudio.com/site/KASReactCalendar/)

To run the examples locally please follow the steps below.

[NPM](https://www.npmjs.com/get-npm) - Install NPM if you currently don't have it


* Install the package from npm

`npm install` and `npm install --only=dev`


* Run on local machine

`npm start`


* Deploy to server

`npm run deploy`


* Check out some simple Jest test

`npm test`


### Features:

* Appointments/Events : These are stored in the localStorage of the browser
* Css Animation : Used to highlight or draw attention to issue
* Display of upcoming event
* Grid Layout : To help for future upgrade to work on mobile
* Jest test using mock localStorage

### Folder Structure:

-src : Main folder for calendar

--Assets : Any images used are located in this folder

--Models : Storage and Calendar helpers are located here

----__TEST__ : Simple Jest test for 2 models

--Sections : Main component sections and styles of the calendar is located here

----__TEST__ : Simple Jest test for 2 sections

--Views : Most of the reusable html is found here


### Component Highlight:

* Moment.js : Used for date modification
* moment-precise-range-plugin : Used to calculate the time left for next event
* uuid : Used to add a unique id to the localStorage
* React Date Picker : Used for the small calendar in edit and add
* lodash : Makes many task easier

## Authors

* **Jose Fonseca** - _[Klip Art Studio](https://www.klipartstudio.com)_ - [GIT](https://gitlab.com/jfonseca/KASReactCalendar.git)

## License

MIT

![calendar](screenshots/calendar.png)

![structure](screenshots/structure.png)